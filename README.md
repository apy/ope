[![build status](https://gitlab.com/apy/ope/badges/master/build.svg)](https://gitlab.com/apy/ope/pipelines)
[![coverage](https://gitlab.com/apy/ope/badges/master/coverage.svg?job=runCoverage)](https://apy.gitlab.io/ope/coverage/index.html)

# ope

## Définition

ope signifie "Outil de Préparation à l'Évaluation".

C'est un outil permettant de préparer l'évaluation de la charte d'un
parc naturel régional.

Il permet de saisir, pour chaque opération menée :

* un état prévisionnel,
* des états réels.

L'évaluation proprement dite, qui est en dehors du périmètre
fonctionnel de ope, consiste alors à comparer les états entre eux.

## Mise en route rapide

Ce paragraphe explique une mise en route rapide pour un environnement
de développement en local.
Pour toute autre utilisation, il convient d'adapter les paramètres de
l'application conformément à la documentation Django :

https://docs.djangoproject.com/fr/1.11/howto/deployment/checklist/

### Installation préalable

Nous avons besoin de peu pour débuter :

* un interpréteur python en version 3,
* un environnement virtuel.

Si vous utilisez une distribution du type Debian GNU/Linux vous
obtiendrez le tout en exécutant la commande suivante :

```bash
sudo aptitude install python3-venv
```

Pour de plus amples informations, se référer à la documentation :

https://docs.djangoproject.com/fr/1.11/intro/install/

### Récupération

Créer et se placer dans son répertoire de développement :

```bash
mkdir dev/
cd dev/
```

Récupérer le projet :

```
git clone https://gitlab.com/apy/ope.git
cd ope/
```

### Environnement virtuel

#### Création

```bash
python3 -m venv venv
```

#### Activation

Débuter le développement :

```bash
source venv/bin/activate
```

Arrêter le développement :

```bash
deactivate
```

### Installation des paquets python nécessaires

```bash
pip install --upgrade pip
pip install -r charter/requirements/production.txt
pip install -r charter/requirements/development.txt
pip install -r charter/requirements/integration.txt
```

### Lancement des tests

```bash
python manage.py test
```

### Lancement du serveur web de développement

Exécuter les deux commandes suivantes lors de la première
utilisation :

```bash
python manage.py migrate
python manage.py createsuperuser
```

Puis par la suite :

```bash
python manage.py runserver
```
