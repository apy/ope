# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from django.utils import \
    formats
from django.test import \
    TestCase
from django.urls import \
    reverse
from job.models import \
    History, \
    Job, \
    JobType, \
    Person
from tree.tests import \
    rnd_str


def add_person(test_1st_name=None,
               test_lst_name=None,
               test_mdl_name=None):
    if test_1st_name is None:
        test_1st_name = \
            rnd_str(Person._meta.get_field('first_name').max_length)
    if test_lst_name is None:
        test_lst_name = \
            rnd_str(Person._meta.get_field('last_name').max_length)
    if test_mdl_name is None:
        test_mdl_name = \
            rnd_str(Person._meta.get_field('middle_name').max_length)
    return Person.objects.create(first_name=test_1st_name,
                                 last_name=test_lst_name,
                                 middle_name=test_mdl_name)


class PersonViewTests(TestCase):

    def test_list(self):
        """
        Checks the response to a list request.
        """
        response = self.client.get(reverse('person:list'))
        self.assertEqual(response.status_code, 200)

    def test_add(self):
        """
        Checks the response to an add request.
        """
        response = self.client.get(reverse('person:add'))
        self.assertEqual(response.status_code, 200)

    def test_see(self):
        """
        Checks the response to a see request.
        """
        test_person = add_person()
        response = self.client.get(reverse('person:see',
                                           args=(test_person.id,)))
        self.assertContains(response, test_person.first_name)
        self.assertContains(response, test_person.last_name)
        self.assertContains(response, test_person.middle_name)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_person = add_person()
        response = self.client.get(reverse('person:edit',
                                           args=(test_person.id,)))
        self.assertContains(response, test_person.first_name)
        self.assertContains(response, test_person.last_name)
        self.assertContains(response, test_person.middle_name)

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_person = add_person()
        response = self.client.get(reverse('person:drop',
                                           args=(test_person.id,)))
        self.assertContains(response, str(test_person))


class PersonMethodTests(TestCase):

    def test_str(self):
        """
        Checks str.
        """
        test_person = add_person()
        self.assertEqual(str(test_person),
                         test_person.first_name + " " +
                         test_person.last_name + " (" +
                         test_person.middle_name + ")")


def add_type(test_label=None):
    if test_label is None:
        test_label = rnd_str(JobType._meta.get_field('label').max_length)
    return JobType.objects.create(label=test_label)


def add_job(test_label=None):
    if test_label is None:
        test_label = rnd_str(Job._meta.get_field('label').max_length)
    return Job.objects.create(label=test_label)


class JobViewTests(TestCase):

    def test_list(self):
        """
        Checks the response to a list request.
        """
        response = self.client.get(reverse('job:list'))
        self.assertEqual(response.status_code, 200)

    def test_add(self):
        """
        Checks the response to an add request.
        """
        response = self.client.get(reverse('job:add'))
        self.assertEqual(response.status_code, 200)

    def test_see(self):
        """
        Checks the response to a see request.
        """
        test_job = add_job()
        response = self.client.get(reverse('job:see',
                                           args=(test_job.id,)))
        self.assertContains(response, test_job.label)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_job = add_job()
        response = self.client.get(reverse('job:edit',
                                           args=(test_job.id,)))
        self.assertContains(response, test_job.label)

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_job = add_job()
        response = self.client.get(reverse('job:drop',
                                           args=(test_job.id,)))
        self.assertContains(response, str(test_job))


def add_history(test_job=None,
                test_type=None,
                test_person=None,
                test_start=None,
                test_end=None):
    if test_job is None:
        test_job = add_job()
    if test_type is None:
        test_type = add_type()
    if test_person is None:
        test_person = add_person()
    if test_start is None:
        test_start = datetime.date.today() - datetime.timedelta(days=1482)
    if test_end is None:
        test_end = test_start + datetime.timedelta(days=1460)
    return History.objects.create(job=test_job,
                                  type=test_type,
                                  person=test_person,
                                  start=test_start,
                                  end=test_end)


class HistoryViewTests(TestCase):

    def test_list(self):
        """
        Checks the response to a list request.
        """
        response = self.client.get(reverse('history:list'))
        self.assertEqual(response.status_code, 200)

    def test_add(self):
        """
        Checks the response to an add request.
        """
        response = self.client.get(reverse('history:add'))
        self.assertEqual(response.status_code, 200)

    def test_see(self):
        """
        Checks the response to a see request.
        """
        test_history = add_history()
        response = self.client.get(reverse('history:see',
                                           args=(test_history.id,)))
        self.assertContains(response, str(test_history.job))
        self.assertContains(response, str(test_history.type))
        self.assertContains(response, str(test_history.person))
        self.assertContains(response,
                            formats.date_format(test_history.start,
                                                "DATE_FORMAT"))
        self.assertContains(response,
                            formats.date_format(test_history.end,
                                                "DATE_FORMAT"))

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        date_format = '%d/%m/%Y'  # locale.nl_langinfo(locale.D_FMT)
        test_history = add_history()
        response = self.client.get(reverse('history:edit',
                                           args=(test_history.id,)))
        self.assertContains(response, str(test_history.job))
        self.assertContains(response, str(test_history.type))
        self.assertContains(response, str(test_history.person))
        self.assertContains(response, test_history.start.strftime(date_format))
        self.assertContains(response, test_history.end.strftime(date_format))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_history = add_history()
        response = self.client.get(reverse('history:drop',
                                           args=(test_history.id,)))
        self.assertContains(response, str(test_history))
