# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import \
    ModelForm
from django.utils.translation import \
    ugettext_lazy as _
from crispy_forms.helper import \
    FormHelper
from crispy_forms.layout import \
    Submit
from job.models import \
    History, \
    Job, \
    JobType, \
    Person


class JobTypeForm(ModelForm):
    class Meta:
        model = JobType
        fields = [
            'label',
        ]

    def __init__(self, *args, **kwargs):
        super(JobTypeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'

        self.helper.add_input(Submit('submit', _('Save')))


class JobForm(ModelForm):
    class Meta:
        model = Job
        fields = [
            'label',
        ]

    def __init__(self, *args, **kwargs):
        super(JobForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'

        self.helper.add_input(Submit('submit', _('Save')))


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = [
            'first_name',
            'last_name',
            'middle_name',
        ]

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'

        self.helper.add_input(Submit('submit', _('Save')))


class HistoryForm(ModelForm):
    class Meta:
        model = History
        fields = [
            'job',
            'type',
            'person',
            'start',
            'end',
        ]

    def __init__(self, *args, **kwargs):
        super(HistoryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'

        self.helper.add_input(Submit('submit', _('Save')))
