# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Admin site for job module
"""

from django.contrib import \
    admin
from job.models import \
    History, \
    Job, \
    JobType, \
    Person


class HistoryInLine(admin.TabularInline):
    model = History
    extra = 0


class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'middle_name')
    search_fields = ['first_name', 'last_name', 'middle_name']
    inlines = [HistoryInLine]


class JobAdmin(admin.ModelAdmin):
    list_display = ('label',)
    search_fields = ['label']
    inlines = [HistoryInLine]


class HistoryAdmin(admin.ModelAdmin):
    list_display = ('job', 'person', 'type', 'start', 'end')
    list_display_links = ('job', 'person')
    list_filter = ('type', 'job')
    date_hierarchy = 'start'
    search_fields = ['job__label',
                     'person__first_name', 'person__last_name']


admin.site.register(Person, PersonAdmin)
admin.site.register(JobType, JobAdmin)
admin.site.register(Job, JobAdmin)
admin.site.register(History, HistoryAdmin)
