��    0      �  C         (     )     .     3     <     A     \  0   o  !   �     �     �  0   �           4     H  	   O     Y     _     g  	   n     x     �     �     �     �     �     �     �     �     �  
   �  	   �     �     �     �  	   �     �     �  	   �     	               $     ,     0     7  
   =     H  �  M     �     �     �     �  L   �     G  ;   Z  O   �  %   �     	  ;   *	     f	     	  
   �	     �	  
   �	  	   �	     �	     �	  
   �	     �	     �	  	   �	  	   	
  	   
     
     )
     1
     =
     I
     Q
     i
     �
     �
     �
  	   �
  	   �
     �
     �
  
   �
     �
  	   �
  
   �
     
          '     6                *      ,      )                       $          (   .          &   -      	                 '      %                /                 0              +             !                           #             
              "    List Next Previous Save The end date may be empty. The list is empty. The middle name allow to differentiate homonyms. The start date must not be empty. Unique name of job type. Unique name of job. You should use the following add functionnality. ^(?P<pk>\d+)/drop/$ ^(?P<pk>\d+)/edit/$ ^add/$ ^history/ ^job/ ^list/$ ^node/ ^partner/ ^person/ add cancel confirm drop drop element droping edit end end date first name histories history job job type job types jobs label last name middle name partner person persons see seeing start start date type Project-Id-Version: 2016
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-31 12:53+0100
PO-Revision-Date: 2016-01-31 11:16+0100
Last-Translator: Adrien Panay <adrien.panay@free.fr>
Language-Team: français <>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.7
Plural-Forms: nplurals=2; plural=(n > 1);
 Liste Suivant Précédent Enregistrer La date de fin doit être saisie, et ce selon le format <em>JJ/MM/AAAA</em>. La liste est vide. Le deuxième prénom permet de différencier les homonymes. La date de début doit être saisie, et ce selon le format <em>JJ/MM/AAAA</em>. Intitulé unique de type de fonction. Intitulé unique de fonction. Vous devriez utiliser la fonctionnalité d'ajout ci-après. ^(?P<pk>\d+)/supprimer/$ ^(?P<pk>\d+)/editer/$ ^ajouter/$ ^historique-de-fonction/ ^fonction/ ^lister/$ ^charte/ ^partenaire/ ^personne/ ajouter annuler confirmer supprimer supprimer suppression éditer date de fin date de fin prénom historiques de fonction historique de fonction fonction type de fonction types de fonction fonctions intitulé nom de famille deuxième prénom partenaire personne personnes visualiser visualisation date de début date de début type 