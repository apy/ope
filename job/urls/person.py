# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import \
    url
from django.utils.translation import \
    ugettext_lazy as _
from job.views import \
    PersonList, \
    PersonAdd, \
    PersonSee, \
    PersonEdit, \
    PersonDrop


app_name = 'person'

urlpatterns = [
    url(_(r'^list/$'),
        PersonList.as_view(),
        name='list'),
    url(_(r'^add/$'),
        PersonAdd.as_view(),
        name='add'),
    url(r'^(?P<pk>\d+)/see/$',
        PersonSee.as_view(),
        name='see'),
    url(_(r'^(?P<pk>\d+)/edit/$'),
        PersonEdit.as_view(),
        name='edit'),
    url(_(r'^(?P<pk>\d+)/drop/$'),
        PersonDrop.as_view(),
        name='drop'),
]
