# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('start', models.DateField(verbose_name='beginning date', help_text='The beginning date must not be empty.')),
                ('end', models.DateField(verbose_name='end date', help_text='The end date may be empty.', blank=True, null=True)),
            ],
            options={
                'verbose_name': 'history',
                'verbose_name_plural': 'histories',
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('label', models.CharField(verbose_name='label', help_text='The label is unique among job labels.', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'job',
                'verbose_name_plural': 'jobs',
            },
        ),
        migrations.CreateModel(
            name='JobType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('label', models.CharField(verbose_name='label', help_text='The label is unique among job type labels.', max_length=15, unique=True)),
            ],
            options={
                'verbose_name': 'job type',
                'verbose_name_plural': 'job types',
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('first_name', models.CharField(verbose_name='first name', help_text='The combination of a first name and a last name is unique.', max_length=25)),
                ('last_name', models.CharField(verbose_name='last name', help_text='The combination of a first name and a last name is unique.', max_length=25)),
                ('middle_name', models.CharField(verbose_name='middle name', help_text='The middle name allow to differentiate two homonym persons.', max_length=25, blank=True)),
            ],
            options={
                'verbose_name': 'person',
                'verbose_name_plural': 'persons',
            },
        ),
        migrations.AlterUniqueTogether(
            name='person',
            unique_together=set([('first_name', 'last_name', 'middle_name')]),
        ),
        migrations.AddField(
            model_name='history',
            name='job',
            field=models.ForeignKey(verbose_name='job', to='job.Job',on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='history',
            name='person',
            field=models.ForeignKey(verbose_name='person', to='job.Person',on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='history',
            name='type',
            field=models.ForeignKey(verbose_name='job type', to='job.JobType',on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='history',
            unique_together=set([('job', 'type', 'person', 'start')]),
        ),
    ]
