# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import \
    reverse_lazy
from django.views.generic import \
    DetailView, \
    ListView
from django.views.generic.edit import \
    CreateView, \
    DeleteView, \
    UpdateView
from job.models import \
    History, \
    Job, \
    Person
from job.forms import \
    HistoryForm, \
    JobForm, \
    PersonForm


class PersonList(ListView):
    model = Person
    paginate_by = 20
    template_name = 'list.html'


class PersonAdd(CreateView):
    model = Person
    form_class = PersonForm
    template_name = 'form.html'


class PersonSee(DetailView):
    model = Person


class PersonEdit(UpdateView):
    model = Person
    form_class = PersonForm
    template_name = 'form.html'


class PersonDrop(DeleteView):
    model = Person
    success_url = reverse_lazy('person:list')
    template_name = 'drop.html'


class JobList(ListView):
    model = Job
    paginate_by = 20
    template_name = 'list.html'


class JobAdd(CreateView):
    model = Job
    form_class = JobForm
    template_name = 'form.html'


class JobSee(DetailView):
    model = Job


class JobEdit(UpdateView):
    model = Job
    form_class = JobForm
    template_name = 'form.html'


class JobDrop(DeleteView):
    model = Job
    success_url = reverse_lazy('job:list')
    template_name = 'drop.html'


class HistoryList(ListView):
    model = History
    paginate_by = 20
    template_name = 'list.html'


class HistoryAdd(CreateView):
    model = History
    form_class = HistoryForm
    template_name = 'form.html'


class HistorySee(DetailView):
    model = History


class HistoryEdit(UpdateView):
    model = History
    form_class = HistoryForm
    template_name = 'form.html'


class HistoryDrop(DeleteView):
    model = History
    success_url = reverse_lazy('history:list')
    template_name = 'drop.html'
