# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Describes the human means of operation related to the park.
this gathers data corresponding to person, job and the history
between them.
"""

from django.db import \
    models
from django.urls import \
    reverse
from django.utils.translation import \
    ugettext_lazy as _


class Person(models.Model):

    """
    A person is someone who is involved on operations related to the
    park.
    """

    first_name = models.CharField(
        verbose_name=_('first name'),
        max_length=25)

    last_name = models.CharField(
        verbose_name=_('last name'),
        max_length=25)

    middle_name = models.CharField(
        verbose_name=_('middle name'),
        max_length=25,
        blank=True,
        help_text=_(
            'The middle name allow to differentiate homonyms.'))

    class Meta:
        verbose_name = _('person')
        verbose_name_plural = _('persons')
        unique_together = (("first_name", "last_name", "middle_name"),)
        ordering = ['first_name', 'last_name']

    def __str__(self):
        result = self.first_name + " " + self.last_name
        if self.middle_name:
            result += " (" + self.middle_name + ")"
        return result

    def get_absolute_url(self):
        return reverse('person:list')


class JobType(models.Model):

    """
    A type of job is a category of occupation that someone performs
    on operation related to the park.
    """

    label = models.CharField(
        max_length=15,
        unique=True,
        verbose_name=_('label'),
        help_text=_('Unique name of job type.'))

    class Meta:
        verbose_name = _('job type')
        verbose_name_plural = _('job types')

    def __str__(self):
        return self.label


class Job(models.Model):

    """
    A job is an occupation that someone performs on operation related
    to the park.
    """

    label = models.CharField(
        max_length=100,
        unique=True,
        verbose_name=_('label'),
        help_text=_('Unique name of job.'))

    class Meta:
        verbose_name = _('job')
        verbose_name_plural = _('jobs')
        ordering = ['label']

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('job:list')


class History(models.Model):

    """
    An history is a mean to know who occupies which job and when.
    """

    job = models.ForeignKey(
        Job,
        verbose_name=_('job'),
        on_delete=models.CASCADE)

    type = models.ForeignKey(
        JobType,
        verbose_name=_('job type'),
        on_delete=models.CASCADE)

    person = models.ForeignKey(
        Person,
        verbose_name=_('person'),
        on_delete=models.CASCADE)

    start = models.DateField(
        verbose_name=_('start date'),
        help_text=_('The start date must not be empty.'))

    end = models.DateField(
        null=True,
        blank=True,
        verbose_name=_('end date'),
        help_text=_('The end date may be empty.'))

    class Meta:
        verbose_name = _('history')
        verbose_name_plural = _('histories')
        unique_together = (("job", "type", "person", "start"),)
        ordering = ['person']

    def __str__(self):
        return str(self.person) + ", " + str(self.job)

    def get_absolute_url(self):
        return reverse('history:list')
