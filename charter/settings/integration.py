# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Load development settings and, eventually,
# Override them with continuous integration ones
from .development import *


INSTALLED_APPS += (
    'django_jenkins',
)


# Continuous integration

# List of applications tested when './manage.py jenkins' command
PROJECT_APPS = (
    'partner',
    'job',
    'tree',
    'identity',
    'state',
)

# List of Jenkins tasks executed by './manage.py jenkins' command

JENKINS_TASKS = (
    'django_jenkins.tasks.run_sloccount',
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pyflakes',
    # 'django_jenkins.tasks.run_jslint',
    # 'django_jenkins.tasks.run_csslint'
)

PYLINT_LOAD_PLUGIN = (
    'pylint_django',
)
