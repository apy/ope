# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Load default settings and, eventually,
# Override them with development ones
from .defaults import *


# SECURITY WARNING: keep the secret key used in production secret!
# The following secret key is intended to ease starting a development
# environment from scratch.
# /!\ DO NOT KEEP IT IN PRODUCTION ENVIRONMENT /!\
SECRET_KEY = 'l-#3m^wx==%qk_rna%1&p$ska-_)!b9&$y6)hi$r1@0_u6fm%^'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
}

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

CRISPY_FAIL_SILENTLY = not DEBUG

INSTALLED_APPS += (
    'django_extensions',
)
