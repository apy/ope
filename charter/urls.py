# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import \
    include, \
    url
from django.contrib import \
    admin
from django.utils.translation import \
    ugettext_lazy as _
from django.views.generic import \
    TemplateView


urlpatterns = [
    url(r'^$',
        TemplateView.as_view(template_name="home.html"),
        name="home"),
    url(r'^admin/',
        admin.site.urls),


    url(_(r'^history/'),
        include('job.urls.history')),
    url(_(r'^person/'),
        include('job.urls.person')),
    url(_(r'^job/'),
        include('job.urls.job')),

    url(_(r'^partner/'),
        include('partner.urls')),

    url(_(r'^node/'),
        include('state.urls')),
]
