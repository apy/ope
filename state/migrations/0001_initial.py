# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('identity', '0001_initial'),
        ('partner', '0001_initial'),
        ('job', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountLine',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(max_length=100, verbose_name='label')),
                ('amount', models.IntegerField(help_text='The amount in €uros.', verbose_name='amount')),
            ],
            options={
                'verbose_name': 'account line',
                'verbose_name_plural': 'account lines',
            },
        ),
        migrations.CreateModel(
            name='Funding',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
            ],
            options={
                'verbose_name': 'funding',
                'verbose_name_plural': 'fundings',
            },
        ),
        migrations.CreateModel(
            name='HumanMean',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('full_time_days_nb', models.PositiveSmallIntegerField(verbose_name='full time days number')),
                ('history', models.ForeignKey(to='job.History', verbose_name='history', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'human mean',
                'verbose_name_plural': 'human means',
            },
        ),
        migrations.CreateModel(
            name='HumanMeanType',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(unique=True, help_text='The label is unique among human mean types.', max_length=15, verbose_name='label')),
            ],
            options={
                'verbose_name': 'human mean type',
                'verbose_name_plural': 'human mean types',
            },
        ),
        migrations.CreateModel(
            name='Partnership',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('partner', models.ForeignKey(to='partner.Partner', verbose_name='partner', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'partnership',
                'verbose_name_plural': 'partnerships',
            },
        ),
        migrations.CreateModel(
            name='PartnershipType',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(unique=True, help_text='The label is unique among partnership type labels.', max_length=50, verbose_name='label')),
            ],
            options={
                'verbose_name': 'partnership type',
                'verbose_name_plural': 'partnership types',
            },
        ),
        migrations.CreateModel(
            name='Procedure',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(unique=True, help_text='The label is a name which is unique among procedure names.', max_length=100, verbose_name='label')),
            ],
            options={
                'verbose_name': 'procedure',
                'verbose_name_plural': 'procedures',
            },
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(help_text='A quantified result corresponds to the measure of an indicator.', max_length=225, verbose_name='quantified result')),
                ('indicator', models.ForeignKey(to='identity.Indicator', verbose_name='indicator', help_text='The operation indicator concerned by this quantified result.', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'result',
                'verbose_name_plural': 'results',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('context', models.CharField(help_text='The context represents the reasons which justify this particular operation.', max_length=5000, verbose_name='context and reason')),
                ('description', models.CharField(help_text='As opposed to long-term purpose, the content represents a description of the goal which is targetted during the time of the operation', max_length=5000, verbose_name='description and content')),
                ('identity', models.ForeignKey(to='identity.Identity', verbose_name='operation identity', help_text='The park operation identity from which to build up this state.', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'state',
                'verbose_name_plural': 'states',
            },
        ),
        migrations.CreateModel(
            name='StateType',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(unique=True, help_text='The label is a name which is unique among state type names.', max_length=25, verbose_name='label')),
            ],
            options={
                'verbose_name': 'state type',
                'verbose_name_plural': 'state types',
            },
        ),
        migrations.CreateModel(
            name='Step',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('label', models.CharField(max_length=100, verbose_name='label')),
                ('start', models.DateField(help_text='The start date must not be empty.', verbose_name='start date')),
                ('end', models.DateField(help_text='The end date must not be empty.', verbose_name='end date')),
                ('state', models.ForeignKey(to='state.State', verbose_name='state', help_text='The operation concerned by this step.', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'step',
                'verbose_name_plural': 'steps',
            },
        ),
        migrations.AddField(
            model_name='state',
            name='type',
            field=models.ForeignKey(to='state.StateType', verbose_name='type', help_text='The type is a category of state which shares a common nature.', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='result',
            name='state',
            field=models.ForeignKey(to='state.State', verbose_name='state', help_text='The operation state concerned by this quantified result.', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='partnership',
            name='state',
            field=models.ForeignKey(to='state.State', verbose_name='state', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='partnership',
            name='type',
            field=models.ForeignKey(to='state.PartnershipType', verbose_name='partnership type', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='humanmean',
            name='state',
            field=models.ForeignKey(to='state.State', verbose_name='state', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='humanmean',
            name='type',
            field=models.ForeignKey(to='state.HumanMeanType', verbose_name='human mean type', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='funding',
            name='procedure',
            field=models.ForeignKey(to='state.Procedure', verbose_name='procedure', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='funding',
            name='state',
            field=models.ForeignKey(to='state.State', verbose_name='state', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='accountline',
            name='state',
            field=models.ForeignKey(to='state.State', verbose_name='state', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='partnership',
            unique_together=set([('state', 'partner', 'type')]),
        ),
        migrations.AlterUniqueTogether(
            name='humanmean',
            unique_together=set([('state', 'history', 'type')]),
        ),
        migrations.AlterUniqueTogether(
            name='funding',
            unique_together=set([('state', 'procedure')]),
        ),
    ]
