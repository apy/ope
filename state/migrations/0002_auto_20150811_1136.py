# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('state', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='accountline',
            unique_together=set([('state', 'label')]),
        ),
        migrations.AlterUniqueTogether(
            name='result',
            unique_together=set([('state', 'indicator')]),
        ),
        migrations.AlterUniqueTogether(
            name='step',
            unique_together=set([('state', 'label')]),
        ),
    ]
