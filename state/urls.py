# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import \
    include, \
    url
from django.utils.translation import \
    ugettext_lazy as _
from state.views.identity import \
    IndicatorAdd, IndicatorEdit, IndicatorDrop, \
    IdentityAdd, IdentityEdit
from state.views.state import \
    AccountLineAdd, AccountLineEdit, AccountLineDrop, \
    ExtensionAdd, ExtensionEdit, ExtensionDrop, \
    FundingAdd, FundingEdit, FundingDrop, \
    HumanMeanAdd, HumanMeanEdit, HumanMeanDrop, \
    PartnershipAdd, PartnershipEdit, PartnershipDrop, \
    ResultAdd, ResultEdit, ResultDrop, \
    StateAdd, StateSee, StateEdit, StateExport, \
    StepAdd, StepEdit, StepDrop, \
    StateTechFile
from state.views.tree import \
    NodeAdd, NodeSee, NodeEdit


app_name = 'node'

aline_urlpatterns = ([
    url(_(r'^add/$'),
        AccountLineAdd.as_view(), name='add'),
    url(_(r'^(?P<aline_pk>\d+)/edit/$'),
        AccountLineEdit.as_view(), name='edit'),
    url(_(r'^(?P<aline_pk>\d+)/drop/$'),
        AccountLineDrop.as_view(), name='drop'),
], 'accountline')

extension_urlpatterns = ([
    url(_(r'^add/$'),
        ExtensionAdd.as_view(), name='add'),
    url(_(r'^(?P<ext_pk>\d+)/edit/$'),
        ExtensionEdit.as_view(), name='edit'),
    url(_(r'^(?P<ext_pk>\d+)/drop/$'),
        ExtensionDrop.as_view(), name='drop'),
], 'extension')

funding_urlpatterns = ([
    url(_(r'^add/$'),
        FundingAdd.as_view(), name='add'),
    url(_(r'^(?P<fund_pk>\d+)/edit/$'),
        FundingEdit.as_view(), name='edit'),
    url(_(r'^(?P<fund_pk>\d+)/drop/$'),
        FundingDrop.as_view(), name='drop'),
], 'funding')

hmean_urlpatterns = ([
    url(_(r'^add/$'),
        HumanMeanAdd.as_view(), name='add'),
    url(_(r'^(?P<hmean_pk>\d+)/edit/$'),
        HumanMeanEdit.as_view(), name='edit'),
    url(_(r'^(?P<hmean_pk>\d+)/drop/$'),
        HumanMeanDrop.as_view(), name='drop'),
], 'humanmean')

pship_urlpatterns = ([
    url(_(r'^add/$'),
        PartnershipAdd.as_view(), name='add'),
    url(_(r'^(?P<pship_pk>\d+)/edit/$'),
        PartnershipEdit.as_view(), name='edit'),
    url(_(r'^(?P<pship_pk>\d+)/drop/$'),
        PartnershipDrop.as_view(), name='drop'),
], 'partnership')

result_urlpatterns = ([
    url(_(r'^add/$'),
        ResultAdd.as_view(), name='add'),
    url(_(r'^(?P<result_pk>\d+)/edit/$'),
        ResultEdit.as_view(), name='edit'),
    url(_(r'^(?P<result_pk>\d+)/drop/$'),
        ResultDrop.as_view(), name='drop'),
], 'result')

step_urlpatterns = ([
    url(_(r'^add/$'),
        StepAdd.as_view(), name='add'),
    url(_(r'^(?P<step_pk>\d+)/edit/$'),
        StepEdit.as_view(), name='edit'),
    url(_(r'^(?P<step_pk>\d+)/drop/$'),
        StepDrop.as_view(), name='drop'),
], 'step')

state_urlpatterns = ([
    url(_(r'^add/\?type_id=(?P<type_id>\d+)/$'),
        StateAdd.as_view(), name='add'),
    url(_(r'^(?P<state_pk>\d+)/see/$'),
        StateSee.as_view(), name='see'),
    url(_(r'^(?P<state_pk>\d+)/edit/$'),
        StateEdit.as_view(), name='edit'),
    url(_(r'^(?P<state_pk>\d+)/techfile/$'),
        StateTechFile.as_view(), name='techfile'),
    url(_(r'^(?P<state_pk>\d+)/export/$'),
        StateExport.as_view(), name='export'),
    # url(_(r'^(?P<state_pk>\d+)/exportpdf/$'),
    #     StateExportPdf, name='exportpdf'),
    url(_(r'^(?P<state_pk>\d+)/accountline/'),
        include(aline_urlpatterns)),
    url(_(r'^(?P<state_pk>\d+)/extension/'),
        include(extension_urlpatterns)),
    url(_(r'^(?P<state_pk>\d+)/funding/'),
        include(funding_urlpatterns)),
    url(_(r'^(?P<state_pk>\d+)/humanmean/'),
        include(hmean_urlpatterns)),
    url(_(r'^(?P<state_pk>\d+)/partnership/'),
        include(pship_urlpatterns)),
    url(_(r'^(?P<state_pk>\d+)/result/'),
        include(result_urlpatterns)),
    url(_(r'^(?P<state_pk>\d+)/step/'),
        include(step_urlpatterns)),
], 'state')

indicator_urlpatterns = ([
    url(_(r'^add/$'),
        IndicatorAdd.as_view(), name='add'),
    url(_(r'^(?P<indi_pk>\d+)/edit/$'),
        IndicatorEdit.as_view(), name='edit'),
    url(_(r'^(?P<indi_pk>\d+)/drop/$'),
        IndicatorDrop.as_view(), name='drop'),
], 'indi')

identity_urlpatterns = ([
    url(_(r'^add/$'),
        IdentityAdd.as_view(), name='add'),
    url(_(r'^(?P<iden_pk>\d+)/edit/$'),
        IdentityEdit.as_view(), name='edit'),
    url(_(r'^(?P<iden_pk>\d+)/indicator/'),
        include(indicator_urlpatterns)),
    url(_(r'^(?P<iden_pk>\d+)/state/'),
        include(state_urlpatterns)),
], 'iden')

urlpatterns = [
    url(_(r'^(?P<node_pk>\d+)/add/$'),
        NodeAdd.as_view(), name='add'),
    url(_(r'^(?P<node_pk>\d+)/see/$'),
        NodeSee.as_view(), name='see'),
    url(_(r'^(?P<node_pk>\d+)/edit/$'),
        NodeEdit.as_view(), name='edit'),
    url(_(r'^(?P<node_pk>\d+)/identity/'),
        include(identity_urlpatterns)),
]
