# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Admin site for state module.
"""

from django.contrib import \
    admin
from state.models import \
    StateType, State, \
    HumanMeanType, HumanMean, \
    PartnershipType, Partnership, \
    Funding, Procedure, \
    Extension, Locality, \
    AccountLine, \
    Step, \
    Result


class ExtensionInLine(admin.TabularInline):
    model = Extension
    extra = 2


class FundingInLine(admin.TabularInline):
    model = Funding
    extra = 2


class AccountLineInLine(admin.TabularInline):
    model = AccountLine
    extra = 2


class PartnershipInLine(admin.TabularInline):
    model = Partnership
    extra = 5


class HumanMeanInLine(admin.TabularInline):
    model = HumanMean
    extra = 2


class StepInLine(admin.TabularInline):
    model = Step
    extra = 2


class ResultInLine(admin.TabularInline):
    model = Result
    extra = 2


class LocalityAdmin(admin.ModelAdmin):
    list_display = ('label',)
    search_fields = ['label']
    inlines = [ExtensionInLine]


class StateAdmin(admin.ModelAdmin):
    list_display = ('type', 'identity', 'last_update', 'locked')
    list_display_links = ('type', 'identity')
    list_editable = ('locked',)
    list_filter = ('type', 'identity__node__type', 'locked')
    search_fields = ['identity__node__label']
    date_hierarchy = 'last_update'
    fields = ('identity', 'type', 'context', 'description', 'locked')
    inlines = [FundingInLine,
               ExtensionInLine,
               AccountLineInLine,
               PartnershipInLine,
               HumanMeanInLine,
               StepInLine,
               ResultInLine]


# Defaults
admin.site.register(AccountLine)
admin.site.register(StateType)
admin.site.register(HumanMeanType)
admin.site.register(HumanMean)
admin.site.register(PartnershipType)
admin.site.register(Partnership)
admin.site.register(Procedure)
admin.site.register(Step)
admin.site.register(Result)
# Custom
admin.site.register(Locality, LocalityAdmin)
admin.site.register(State, StateAdmin)
