# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import \
    reverse
from django.utils.text import \
    slugify
from django.views.generic import \
    DetailView
from django.views.generic.edit import \
    CreateView, \
    DeleteView, \
    UpdateView
from identity.models import \
    Identity, \
    Indicator
from state.models import \
    AccountLine, \
    Extension, \
    Funding, \
    HumanMean, \
    Partnership, \
    Result, \
    State, \
    StateType, \
    Step
from state.forms import \
    AccountLineForm, \
    ExtensionForm, \
    FundingForm, \
    HumanMeanForm, \
    PartnershipForm, \
    ResultForm, \
    StateForm, \
    StepForm


class AccountLineAdd(CreateView):
    model = AccountLine
    form_class = AccountLineForm
    pk_url_kwarg = 'aline_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(AccountLineAdd, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.kwargs['state_pk'])
        return form

    def get_initial(self):
        initial = super(AccountLineAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class AccountLineEdit(UpdateView):
    model = AccountLine
    form_class = AccountLineForm
    pk_url_kwarg = 'aline_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(AccountLineEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        return form


class AccountLineDrop(DeleteView):
    model = AccountLine
    pk_url_kwarg = 'aline_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))


class ExtensionAdd(CreateView):
    model = Extension
    form_class = ExtensionForm
    pk_url_kwarg = 'ext_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(ExtensionAdd, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.kwargs['state_pk'])
        return form

    def get_initial(self):
        initial = super(ExtensionAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class ExtensionEdit(UpdateView):
    model = Extension
    form_class = ExtensionForm
    pk_url_kwarg = 'ext_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(ExtensionEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        return form


class ExtensionDrop(DeleteView):
    model = Extension
    pk_url_kwarg = 'ext_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))


class FundingAdd(CreateView):
    model = Funding
    form_class = FundingForm
    pk_url_kwarg = 'fund_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(FundingAdd, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.kwargs['state_pk'])
        return form

    def get_initial(self):
        initial = super(FundingAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class FundingEdit(UpdateView):
    model = Funding
    form_class = FundingForm
    pk_url_kwarg = 'fund_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(FundingEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        return form


class FundingDrop(DeleteView):
    model = Funding
    pk_url_kwarg = 'fund_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))


class HumanMeanAdd(CreateView):
    model = HumanMean
    form_class = HumanMeanForm
    pk_url_kwarg = 'hmean_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(HumanMeanAdd, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.kwargs['state_pk'])
        return form

    def get_initial(self):
        initial = super(HumanMeanAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class HumanMeanEdit(UpdateView):
    model = HumanMean
    form_class = HumanMeanForm
    pk_url_kwarg = 'hmean_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(HumanMeanEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        return form


class HumanMeanDrop(DeleteView):
    model = HumanMean
    pk_url_kwarg = 'hmean_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))


class PartnershipAdd(CreateView):
    model = Partnership
    form_class = PartnershipForm
    pk_url_kwarg = 'pship_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(PartnershipAdd, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.kwargs['state_pk'])
        return form

    def get_initial(self):
        initial = super(PartnershipAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class PartnershipEdit(UpdateView):
    model = Partnership
    form_class = PartnershipForm
    pk_url_kwarg = 'pship_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(PartnershipEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        return form


class PartnershipDrop(DeleteView):
    model = Partnership
    pk_url_kwarg = 'pship_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))


class ResultAdd(CreateView):
    model = Result
    form_class = ResultForm
    pk_url_kwarg = 'result_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(ResultAdd, self).get_form()
        state_id = self.kwargs['state_pk']

        indicators = Indicator.objects.filter(
            identity=Identity.objects.get(
                id=self.kwargs['iden_pk']))
        used_indicators = Result.objects.filter(
            state=State.objects.get(
                id=state_id)).values('indicator_id')
        available_indicators = indicators.exclude(
                id__in=used_indicators)

        form.fields['state'].queryset = \
            State.objects.filter(id=state_id)
        form.fields['indicator'].queryset = available_indicators
        return form

    def get_initial(self):
        initial = super(ResultAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class ResultEdit(UpdateView):
    model = Result
    form_class = ResultForm
    pk_url_kwarg = 'result_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(ResultEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        form.fields['indicator'].queryset = \
            Indicator.objects.filter(id=self.object.indicator.id)
        return form


class ResultDrop(DeleteView):
    model = Result
    pk_url_kwarg = 'result_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))


# def StateExportPdf(request, node_pk, iden_pk, state_pk):
#     from django.http import HttpResponse
#     from io import BytesIO
#     from reportlab.pdfgen import canvas

#     state_obj = State.objects.get(id=state_pk)
#     date_str = state_obj.last_update.strftime('%Y-%m-%d')
#     type_str = str(state_obj.type)
#     name_str = str(state_obj.identity.node)

#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = \
#         'inline; filename="' + \
#         date_str + '_' + type_str + '_' + name_str + '"'
#     pdf_doc_buffer = BytesIO()
#     p = canvas.Canvas(response)
#     p.setTitle(date_str + ' ' + type_str + ' ' + name_str)
#     p.drawString(100, 100, "Hello world.")
#     p.showPage()
#     p.save()
#     pdf_doc = pdf_doc_buffer.getvalue()
#     pdf_doc_buffer.close()
#     response.write(pdf_doc)
#     return response


class StateTechFile(DetailView):
    model = State
    pk_url_kwarg = 'state_pk'
    template_name = 'state/state_tech_file.html'


class StateExport(DetailView):
    model = State
    pk_url_kwarg = 'state_pk'
    template_name = 'state/state_export.html'


class StateSee(DetailView):
    model = State
    pk_url_kwarg = 'state_pk'


class StateAdd(CreateView):
    model = State
    form_class = StateForm
    pk_url_kwarg = 'state_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(StateAdd, self).get_form()
        form.fields['identity'].queryset = \
            Identity.objects.filter(id=self.kwargs['iden_pk'])
        form.fields['type'].queryset = \
            StateType.objects.filter(id=self.kwargs['type_id'])
        return form

    def get_initial(self):
        initial = super(StateAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'identity':
                Identity.objects.get(id=self.kwargs['iden_pk'])})
        initial.update(
            {'type':
                StateType.objects.get(id=self.kwargs['type_id'])})
        return initial


class StateEdit(UpdateView):
    model = State
    form_class = StateForm
    pk_url_kwarg = 'state_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(StateEdit, self).get_form()
        form.fields['identity'].queryset = \
            Identity.objects.filter(id=self.object.identity.id)
        form.fields['type'].queryset = \
            StateType.objects.filter(id=self.object.type.id)
        return form


class StepAdd(CreateView):
    model = Step
    form_class = StepForm
    pk_url_kwarg = 'step_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(StepAdd, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.kwargs['state_pk'])
        return form

    def get_initial(self):
        initial = super(StepAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'state':
                State.objects.get(id=self.kwargs['state_pk'])})
        return initial


class StepEdit(UpdateView):
    model = Step
    form_class = StepForm
    pk_url_kwarg = 'step_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(StepEdit, self).get_form()
        form.fields['state'].queryset = \
            State.objects.filter(id=self.object.state.id)
        return form


class StepDrop(DeleteView):
    model = Step
    pk_url_kwarg = 'step_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:iden:state:see',
                       args=[self.kwargs['node_pk'],
                             self.kwargs['iden_pk'],
                             self.kwargs['state_pk']]) + \
               '#' + str(slugify(self.model._meta.verbose_name_plural))
