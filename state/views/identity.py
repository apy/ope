# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import \
    reverse
from django.views.generic.edit import \
    CreateView, \
    DeleteView, \
    UpdateView
from tree.models import \
    Node
from identity.models import \
    Identity, \
    Indicator
from identity.forms import \
    IdentityForm, \
    IndicatorForm


class IdentityAdd(CreateView):
    model = Identity
    form_class = IdentityForm
    pk_url_kwarg = 'iden_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(IdentityAdd, self).get_form()
        form.fields['node'].queryset = \
            Node.objects.filter(id=self.kwargs['node_pk'])
        return form

    def get_initial(self):
        initial = super(IdentityAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'node':
                Node.objects.get(id=self.kwargs['node_pk'])})
        return initial


class IdentityEdit(UpdateView):
    model = Identity
    form_class = IdentityForm
    pk_url_kwarg = 'iden_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(IdentityEdit, self).get_form()
        form.fields['node'].queryset = \
            Node.objects.filter(id=self.object.node.id)
        return form


class IndicatorAdd(CreateView):
    model = Indicator
    form_class = IndicatorForm
    pk_url_kwarg = 'indi_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(IndicatorAdd, self).get_form()
        form.fields['identity'].queryset = \
            Identity.objects.filter(id=self.kwargs['iden_pk'])
        return form

    def get_initial(self):
        initial = super(IndicatorAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'identity':
                Identity.objects.get(id=self.kwargs['iden_pk'])})
        return initial


class IndicatorEdit(UpdateView):
    model = Indicator
    form_class = IndicatorForm
    pk_url_kwarg = 'indi_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(IndicatorEdit, self).get_form()
        form.fields['identity'].queryset = \
            Identity.objects.filter(id=self.object.identity.id)
        return form


class IndicatorDrop(DeleteView):
    model = Indicator
    pk_url_kwarg = 'indi_pk'
    template_name = 'drop.html'

    def get_success_url(self):
        return reverse('node:see',
                       args=[self.kwargs['node_pk']]) + \
               '#' + str(self.model._meta.verbose_name_plural)
