# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import \
    DetailView
from django.views.generic.edit import \
    CreateView, \
    UpdateView
from tree.models import \
    Node, \
    NodeType
from tree.forms import \
    NodeForm


class NodeSee(DetailView):
    model = Node
    pk_url_kwarg = 'node_pk'


class NodeAdd(CreateView):
    model = Node
    form_class = NodeForm
    pk_url_kwarg = 'node_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(NodeAdd, self).get_form()
        parent_id = self.kwargs['node_pk']
        type_id = Node.objects.get(id=parent_id).type.id + 1
        form.fields['parent'].queryset = \
            Node.objects.filter(id=parent_id)
        form.fields['type'].queryset = \
            NodeType.objects.filter(id=type_id)
        return form

    def get_initial(self):
        parent_id = self.kwargs['node_pk']
        type_id = Node.objects.get(id=parent_id).type.id + 1
        initial = super(NodeAdd, self).get_initial()
        initial = initial.copy()
        initial.update(
            {'parent':
                Node.objects.get(id=parent_id)})
        initial.update(
            {'type':
                NodeType.objects.get(id=type_id)})
        return initial


class NodeEdit(UpdateView):
    model = Node
    form_class = NodeForm
    pk_url_kwarg = 'node_pk'
    template_name = 'form.html'

    def get_form(self):
        form = super(NodeEdit, self).get_form()
        if not self.object.is_root_node():
            form.fields['parent'].queryset = \
                Node.objects.filter(id=self.object.parent.id)
        else:
            form.fields['parent'].empty_label = '---------'
            form.fields['parent'].queryset = Node.objects.none()
        form.fields['type'].queryset = \
            NodeType.objects.filter(id=self.object.type.id)
        return form
