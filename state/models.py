# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Describes the dynamic informations of operation related to the park.
A dynamic information evolves during an operation.
"""

from django.db import \
    models
from django.db.models import \
    Sum
from django.urls import \
    reverse
from django.utils.translation import \
    ugettext_lazy as _
from django.utils.text import \
    slugify
from identity.models import \
    Identity, \
    Indicator
from job.models import \
    History
from partner.models import \
    Partner


class StateType(models.Model):

    """
    A state type represents the nature of a state:
    prévision, réalisation.
    """

    label = models.CharField(
        max_length=25,
        unique=True,
        verbose_name=_('label'),
        help_text=_(
            'Unique name of the nature of state.'))

    class Meta:
        verbose_name = _('state type')
        verbose_name_plural = _('state types')

    def __str__(self):
        return self.label


class State(models.Model):

    """
    A state gathers the dynamic informations of an operation.
    """

    identity = models.ForeignKey(
        Identity,
        verbose_name=_('node'),
        help_text=_(
            'Operation concerned by this dynamic informations.'),
        on_delete=models.CASCADE)

    type = models.ForeignKey(
        StateType,
        verbose_name=_('type'),
        help_text=_(
            'Nature of the operation state.'),
        on_delete=models.CASCADE)

    context = models.CharField(
        max_length=5000,
        verbose_name=_('context and reason'),
        help_text=_(
            'Context and reasons which justify the operation.'))

    description = models.CharField(
        max_length=5000,
        verbose_name=_('description and content'),
        help_text=_(
            'As opposed to long-term purpose, the content represents '
            'a description of the goal which is targetted during the '
            'time of the operation.'))

    locked = models.BooleanField(
        default=False,
        verbose_name=_('locked'),
        help_text=_(
            'Indicates that editing the state is not allowed.'))

    last_update = models.DateTimeField(
        auto_now=True,
        verbose_name=_('last update'),
        help_text=_(
            'Indicates the last time the state was updated.'))

    class Meta:
        verbose_name = _('state')
        verbose_name_plural = _('states')

    def __str__(self):
        result = \
            self.last_update.strftime('%d/%m/%Y') + ' : ' + \
            str(self.type) + ' ' + \
            self.identity.node.short_str()
        return result

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.identity.node.id,
                               'iden_pk': self.identity.id,
                               'state_pk': self.id})

    def get_operation_manager(self):
        partnerships = self.partnership_set.filter(type=10)
        return partnerships

    def get_accountlines_sum(self, al_type):
        accountlines = self.accountline_set.filter(type=al_type)
        result = accountlines.aggregate(Sum('amount'))['amount__sum']
        return result

    def get_expenses_sum(self):
        return self.get_accountlines_sum(al_type=AccountLine.EXPENSE)

    def get_incomes_sum(self):
        return self.get_accountlines_sum(al_type=AccountLine.INCOME)

    def get_distinct_partners(self):
        partnerships = \
            self.partnership_set.order_by().values('partner__name')
        distinct_partners = \
            partnerships.distinct().order_by('partner__name')
        return distinct_partners

    def get_humanmeans_sum(self):
        humanmeans = self.humanmean_set
        sum_aggregate = humanmeans.aggregate(Sum('full_time_days_nb'))
        result = sum_aggregate['full_time_days_nb__sum']
        return result


class Result(models.Model):

    """
    A quantified result represents a measure corresponding to an indicator.
    """

    label = models.CharField(
        max_length=225,
        verbose_name=_('quantified result'),
        help_text=_(
            'A quantified result corresponds to the measure of an '
            'indicator.'))

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        help_text=_(
            'Operation concerned by this quantified result.'),
        on_delete=models.CASCADE)

    indicator = models.ForeignKey(
        Indicator,
        verbose_name=_('indicator'),
        help_text=_(
            'Indicator concerned by this quantified result.'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('result')
        verbose_name_plural = _('results')
        unique_together = (("state", "indicator"),)

    def __str__(self):
        return self.label + ' : ' + str(self.indicator)

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))


class Step(models.Model):

    """
    The steps are time slots that divide an operation.
    """

    label = models.CharField(
        max_length=100,
        verbose_name=_('label'))

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        help_text=_('The operation concerned by this step.'),
        on_delete=models.CASCADE)

    start = models.DateField(
        verbose_name=_('start date'),
        help_text=_('The start date must not be empty.'))

    end = models.DateField(
        verbose_name=_('end date'),
        help_text=_('The end date must not be empty.'))

    class Meta:
        verbose_name = _('step')
        verbose_name_plural = _('steps')
        unique_together = (("state", "label"),)
        ordering = ['state__id', 'start', 'end']

    def __str__(self):
        date_format = '%d/%m/%y'  # locale.nl_langinfo(locale.D_FMT)
        result = \
            self.start.strftime(date_format) + ' - ' + \
            self.end.strftime(date_format) + ' : ' + \
            self.label
        return result

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))


class AccountLine(models.Model):

    """
    The account lines allow to detail operation account.
    """

    INCOME = True
    EXPENSE = False
    TYPES = (
        (INCOME, _('income')),
        (EXPENSE, _('expense')))

    label = models.CharField(
        max_length=100,
        verbose_name=_('label'))

    type = models.BooleanField(
        choices=TYPES,
        default=EXPENSE,
        verbose_name=_('expense or income'))

    amount = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        verbose_name=_('amount'),
        help_text=_('The amount in €uros.'))

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('account line')
        verbose_name_plural = _('account lines')
        unique_together = (("state", "label"),)
        ordering = ['type']

    def __str__(self):
        if self.type == self.INCOME:
            result = _('income')
        else:
            result = _('expense')
        result += ' ' + str(self.amount) + '€ : ' + self.label
        return result

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))

    def get_rate(self):
        total = self.state.get_accountlines_sum(al_type=self.type)
        rate = round(self.amount / total * 100, 2)
        return rate


class Locality(models.Model):

    """
    A locality is an area where the park leads its operations.
    """

    label = models.CharField(
        max_length=100,
        unique=True,
        verbose_name=_('label'),
        help_text=_(
            'The label is a name which is unique among locality names.'))

    class Meta:
        verbose_name = _('locality')
        verbose_name_plural = _('localities')
        ordering = ['label']

    def __str__(self):
        return self.label

    # def get_absolute_url(self):
    #     return reverse('procedure:list')


class Extension(models.Model):

    """
    An extension prolongs an operation on a locality.
    """

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        on_delete=models.CASCADE)
    locality = models.ForeignKey(
        Locality,
        verbose_name=_('locality'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('extension')
        verbose_name_plural = _('extensions')
        unique_together = (("state", "locality"),)

    def __str__(self):
        return str(self.locality)

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))


class Procedure(models.Model):

    """
    A procedure allows the park to fund its operations.
    """

    label = models.CharField(
        max_length=100,
        unique=True,
        verbose_name=_('label'),
        help_text=_(
            'The label is a name which is unique among procedure names.'))

    class Meta:
        verbose_name = _('procedure')
        verbose_name_plural = _('procedures')
        ordering = ['label']

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('procedure:list')


class Funding(models.Model):

    """
    A funding provides a financial procedure to an operation.
    """

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        on_delete=models.CASCADE)
    procedure = models.ForeignKey(
        Procedure,
        verbose_name=_('procedure'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('funding')
        verbose_name_plural = _('fundings')
        unique_together = (("state", "procedure"),)

    def __str__(self):
        return str(self.procedure)

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))


class PartnershipType(models.Model):

    """
    A partnership type defines the nature of the participation of an
    institution or an organism on an operation.
    """

    label = models.CharField(
        _('label'),
        max_length=50,
        unique=True,
        help_text=_(
            'The label is unique among partnership type labels.'))

    class Meta:
        verbose_name = _('partnership type')
        verbose_name_plural = _('partnership types')
        ordering = ['label']

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('partnershiptype:list')


class Partnership(models.Model):

    """
    A partnership associates a partner and an operation related to the
    park.
    """

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        on_delete=models.CASCADE)
    partner = models.ForeignKey(
        Partner,
        verbose_name=_('partner'),
        on_delete=models.CASCADE)
    type = models.ForeignKey(
        PartnershipType,
        verbose_name=_('partnership type'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('partnership')
        verbose_name_plural = _('partnerships')
        unique_together = (("state", "partner", "type"),)
        ordering = ['partner', 'type']

    def __str__(self):
        return str(self.partner) + ' : ' + str(self.type)

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))


class HumanMeanType(models.Model):

    """
    A type of human mean is the role of a person on operations related
    to the park.
    """

    label = models.CharField(
        max_length=15,
        unique=True,
        verbose_name=_('label'),
        help_text=_('The label is unique among human mean types.'))

    class Meta:
        verbose_name = _('human mean type')
        verbose_name_plural = _('human mean types')

    def __str__(self):
        return self.label


class HumanMean(models.Model):

    """
    A human mean associates a job history and an operation related to
    the park.
    """

    state = models.ForeignKey(
        State,
        verbose_name=_('state'),
        on_delete=models.CASCADE)
    history = models.ForeignKey(
        History,
        verbose_name=_('history'),
        on_delete=models.CASCADE)
    type = models.ForeignKey(
        HumanMeanType,
        verbose_name=_('human mean type'),
        on_delete=models.CASCADE)
    full_time_days_nb = models.PositiveSmallIntegerField(
        verbose_name=_('full time days number'))

    class Meta:
        verbose_name = _('human mean')
        verbose_name_plural = _('human means')
        unique_together = (("state", "history", "type"),)

    def __str__(self):
        return str(self.type) + ' : ' + str(self.history.person)

    def get_absolute_url(self):
        return reverse('node:iden:state:see',
                       kwargs={'node_pk': self.state.identity.node.id,
                               'iden_pk': self.state.identity.id,
                               'state_pk': self.state.id}) + \
               '#' + str(slugify(self._meta.verbose_name_plural))
