# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django import \
    template
from django.utils.translation import \
    ugettext_lazy as _


register = template.Library()


@register.filter
def order_by(queryset, order):
    return queryset.order_by(order)


@register.filter
def verbose_name(view):
    return view.model._meta.verbose_name


@register.filter
def verbose_name_plural(view):
    return view.model._meta.verbose_name_plural


@register.filter
def add_or_edit_str(obj):
    result = ''
    if obj:
        result = _('editing')
    else:
        result = _('adding')
    return result


@register.filter
def class_name(obj):
    result = ''
    if obj:
        result = obj.__class__.__name__.lower()
    return result


@register.filter
def state_url(obj):
    result = ''
    if obj.state:
        result = 'node:iden:state:'
    return result


@register.filter
def get_url(obj):
    result = class_name(obj)
    if result in ['funding',
                  'step',
                  'result',
                  'partnership',
                  'humanmean',
                  'accountline',
                  'extension']:
        result = state_url(obj) + result
    return result


@register.filter
def see_url(obj):
    result = get_url(obj) + ':see'
    return result


@register.filter
def edit_url(obj):
    result = get_url(obj) + ':edit'
    return result


@register.filter
def drop_url(obj):
    result = get_url(obj) + ':drop'
    return result


@register.filter
def model_name(view):
    return view.model.__name__


@register.filter
def add_url_fqn(view):
    return model_name(view).lower() + ':add'
