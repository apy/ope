# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Describes the state forms.
"""

from django.forms import \
    ModelForm, \
    Textarea
from django.utils.translation import \
    ugettext_lazy as _
from crispy_forms.helper import \
    FormHelper
from crispy_forms.layout import \
    Submit
from state.models import \
    AccountLine, \
    Extension, \
    Funding, \
    HumanMean, \
    Partnership, \
    Result, \
    State, \
    Step


class AccountLineForm(ModelForm):
    class Meta:
        model = AccountLine
        fields = [
            'state',
            'label',
            'type',
            'amount',
        ]

    def __init__(self, *args, **kwargs):
        super(AccountLineForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class ExtensionForm(ModelForm):
    class Meta:
        model = Extension
        fields = [
            'state',
            'locality',
        ]

    def __init__(self, *args, **kwargs):
        super(ExtensionForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class FundingForm(ModelForm):
    class Meta:
        model = Funding
        fields = [
            'state',
            'procedure',
        ]

    def __init__(self, *args, **kwargs):
        super(FundingForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class HumanMeanForm(ModelForm):
    class Meta:
        model = HumanMean
        fields = [
            'state',
            'history',
            'type',
            'full_time_days_nb',
        ]

    def __init__(self, *args, **kwargs):
        super(HumanMeanForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class PartnershipForm(ModelForm):
    class Meta:
        model = Partnership
        fields = [
            'state',
            'partner',
            'type',
        ]

    def __init__(self, *args, **kwargs):
        super(PartnershipForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class StepForm(ModelForm):
    class Meta:
        model = Step
        fields = [
            'state',
            'label',
            'start',
            'end',
        ]
        # widgets = {
        #     'start': SelectDateWidget(),
        #     'end': SelectDateWidget(),
        # }

    def __init__(self, *args, **kwargs):
        super(StepForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class ResultForm(ModelForm):
    class Meta:
        model = Result
        fields = [
            'state',
            'indicator',
            'label',
        ]

    def __init__(self, *args, **kwargs):
        super(ResultForm, self).__init__(*args, **kwargs)
        self.fields['state'].empty_label = None
        self.fields['state'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class StateForm(ModelForm):
    class Meta:
        model = State
        fields = [
            'identity',
            'type',
            'context',
            'description',
        ]
        widgets = {
            'context': Textarea(),
            'description': Textarea(),
        }

    def __init__(self, *args, **kwargs):
        super(StateForm, self).__init__(*args, **kwargs)
        self.fields['identity'].empty_label = None
        self.fields['identity'].widget.attrs['readonly'] = True
        self.fields['type'].empty_label = None
        self.fields['type'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))
