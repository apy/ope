# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import decimal
import random
from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    AccountLine
from state.tests.tests_state import \
    rnd_str, \
    add_state


def add_line(test_label=None,
             test_amount=None,
             test_state=None):
    if test_label is None:
        test_label = rnd_str(AccountLine._meta.get_field('label').max_length)
    if test_amount is None:
        test_amount = decimal.Decimal(random.randint(0, 999999999999))/100
    if test_state is None:
        test_state = add_state()
    return AccountLine.objects.create(label=test_label,
                                      amount=test_amount,
                                      state=test_state)


class AccountLineViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_state = add_state()
        url = reverse('node:iden:state:accountline:add',
                      args=(test_state.identity.node.id,
                            test_state.identity.id,
                            test_state.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_line = add_line()
        url = reverse('node:iden:state:accountline:edit',
                      args=(test_line.state.identity.node.id,
                            test_line.state.identity.id,
                            test_line.state.id,
                            test_line.id))
        response = self.client.get(url)
        self.assertContains(response, test_line.label)
        self.assertContains(response, str(test_line.amount))
        self.assertContains(response, str(test_line.state))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_line = add_line()
        url = reverse('node:iden:state:accountline:drop',
                      args=(test_line.state.identity.node.id,
                            test_line.state.identity.id,
                            test_line.state.id,
                            test_line.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_line))


class AccountLineMethodTests(TestCase):

    def test_state_update(self):
        """
        Checks state update when a relation to it is added.
        """
        test_state = add_state()
        create = test_state.last_update
        add_line(test_state=test_state)
        update = test_state.last_update
        self.assertLess(create, update)
