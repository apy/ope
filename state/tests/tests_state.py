# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    State, \
    StateType
from identity.tests import \
    rnd_str, \
    add_id


def add_type(test_label=None):
    if test_label is None:
        test_label = rnd_str(StateType._meta.get_field('label').max_length)
    return StateType.objects.create(label=test_label)


def add_state(test_id=None,
              test_type=None,
              test_context=None,
              test_desc=None):
    if test_id is None:
        test_id = add_id()
    if test_type is None:
        test_type = add_type()
    if test_context is None:
        test_context = rnd_str(State._meta.get_field('context').max_length)
    if test_desc is None:
        test_desc = rnd_str(State._meta.get_field('description').max_length)
    return State.objects.create(identity=test_id,
                                type=test_type,
                                context=test_context,
                                description=test_desc)


class StateViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_id = add_id()
        test_type = add_type()
        response = self.client.get(reverse('node:iden:state:add',
                                           args=(test_id.node.id,
                                                 test_id.id,
                                                 test_type.id,)))
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_state = add_state()
        response = self.client.get(reverse('node:iden:state:edit',
                                           args=(test_state.identity.node.id,
                                                 test_state.identity.id,
                                                 test_state.id,)))
        self.assertContains(response, str(test_state.identity))
        self.assertContains(response, str(test_state.type))
        self.assertContains(response, test_state.context)
        self.assertContains(response, test_state.description)
