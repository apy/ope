# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    Result
from state.tests.tests_state import \
    rnd_str, \
    add_state
from identity.tests import \
    add_indicator


def add_result(test_label=None,
               test_state=None,
               test_indicator=None):
    if test_label is None:
        test_label = \
            rnd_str(Result._meta.get_field('label').max_length)
    if test_state is None:
        test_state = add_state()
    if test_indicator is None:
        test_indicator = add_indicator()
    return Result.objects.create(label=test_label,
                                 state=test_state,
                                 indicator=test_indicator)


class ResultViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_state = add_state()
        url = reverse('node:iden:state:result:add',
                      args=(test_state.identity.node.id,
                            test_state.identity.id,
                            test_state.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_result = add_result()
        url = reverse('node:iden:state:result:edit',
                      args=(test_result.state.identity.node.id,
                            test_result.state.identity.id,
                            test_result.state.id,
                            test_result.id))
        response = self.client.get(url)
        self.assertContains(response, test_result.label)
        self.assertContains(response, str(test_result.state))
        self.assertContains(response, str(test_result.indicator))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_result = add_result()
        url = reverse('node:iden:state:result:drop',
                      args=(test_result.state.identity.node.id,
                            test_result.state.identity.id,
                            test_result.state.id,
                            test_result.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_result))


class ResultMethodTests(TestCase):

    def test_state_update(self):
        """
        Checks state update when a relation to it is added.
        """
        test_state = add_state()
        create = test_state.last_update
        add_result(test_state=test_state)
        update = test_state.last_update
        self.assertLess(create, update)
