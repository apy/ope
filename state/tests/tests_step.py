# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    Step
from state.tests.tests_state import \
    rnd_str, \
    add_state


def add_step(test_label=None,
             test_state=None,
             test_start=None,
             test_end=None):
    if test_label is None:
        test_label = rnd_str(Step._meta.get_field('label').max_length)
    if test_state is None:
        test_state = add_state()
    if test_start is None:
        test_start = datetime.date.today() - datetime.timedelta(days=700)
    if test_end is None:
        test_end = test_start + datetime.timedelta(days=5000)
    return Step.objects.create(label=test_label,
                               state=test_state,
                               start=test_start,
                               end=test_end)


class StepViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_state = add_state()
        url = reverse('node:iden:state:step:add',
                      args=(test_state.identity.node.id,
                            test_state.identity.id,
                            test_state.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        date_format = '%d/%m/%Y'  # locale.nl_langinfo(locale.D_FMT)
        test_step = add_step()
        url = reverse('node:iden:state:step:edit',
                      args=(test_step.state.identity.node.id,
                            test_step.state.identity.id,
                            test_step.state.id,
                            test_step.id))
        response = self.client.get(url)
        self.assertContains(response, test_step.label)
        self.assertContains(response, str(test_step.state))
        self.assertContains(response, test_step.start.strftime(date_format))
        self.assertContains(response, test_step.end.strftime(date_format))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_step = add_step()
        url = reverse('node:iden:state:step:drop',
                      args=(test_step.state.identity.node.id,
                            test_step.state.identity.id,
                            test_step.state.id,
                            test_step.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_step))


class StepMethodTests(TestCase):

    def test_state_update(self):
        """
        Checks state update when a relation to it is added.
        """
        test_state = add_state()
        create = test_state.last_update
        add_step(test_state=test_state)
        update = test_state.last_update
        self.assertLess(create, update)
