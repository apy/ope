# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    Partnership, \
    PartnershipType
from state.tests.tests_state import \
    rnd_str, \
    add_state
from partner.tests import \
    add_partner


def add_type(test_label=None):
    if test_label is None:
        test_label = \
            rnd_str(PartnershipType._meta.get_field('label').max_length)
    return PartnershipType.objects.create(label=test_label)


def add_partnership(test_state=None,
                    test_partner=None,
                    test_type=None):
    if test_state is None:
        test_state = add_state()
    if test_partner is None:
        test_partner = add_partner()
    if test_type is None:
        test_type = add_type()
    return Partnership.objects.create(state=test_state,
                                      partner=test_partner,
                                      type=test_type)


class PartnershipViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_state = add_state()
        url = reverse('node:iden:state:partnership:add',
                      args=(test_state.identity.node.id,
                            test_state.identity.id,
                            test_state.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_pship = add_partnership()
        url = reverse('node:iden:state:partnership:edit',
                      args=(test_pship.state.identity.node.id,
                            test_pship.state.identity.id,
                            test_pship.state.id,
                            test_pship.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_pship.state))
        self.assertContains(response, str(test_pship.partner))
        self.assertContains(response, str(test_pship.type))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_pship = add_partnership()
        url = reverse('node:iden:state:partnership:drop',
                      args=(test_pship.state.identity.node.id,
                            test_pship.state.identity.id,
                            test_pship.state.id,
                            test_pship.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_pship))


class PartnershipMethodTests(TestCase):

    def test_state_update(self):
        """
        Checks state update when a relation to it is added.
        """
        test_state = add_state()
        create = test_state.last_update
        add_partnership(test_state=test_state)
        update = test_state.last_update
        self.assertLess(create, update)
