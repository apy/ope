# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from tree.tests import \
    add_node, \
    add_type


class NodeViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_parent = add_node()
        test_type = add_type()
        response = self.client.get(reverse('node:add',
                                           args=(test_parent.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, test_type.label)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_node = add_node()
        response = self.client.get(reverse('node:edit',
                                           args=(test_node.id,)))
        self.assertContains(response, test_node.label)
