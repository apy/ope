# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    Extension, \
    Locality
from state.tests.tests_state import \
    rnd_str, \
    add_state


def add_locality(test_label=None):
    if test_label is None:
        test_label = rnd_str(Locality._meta.get_field('label').max_length)
    return Locality.objects.create(label=test_label)


def add_extension(test_state=None,
                  test_loc=None):
    if test_state is None:
        test_state = add_state()
    if test_loc is None:
        test_loc = add_locality()
    return Extension.objects.create(state=test_state,
                                    locality=test_loc)


class ExtensionViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_state = add_state()
        url = reverse('node:iden:state:extension:add',
                      args=(test_state.identity.node.id,
                            test_state.identity.id,
                            test_state.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_ext = add_extension()
        url = reverse('node:iden:state:extension:edit',
                      args=(test_ext.state.identity.node.id,
                            test_ext.state.identity.id,
                            test_ext.state.id,
                            test_ext.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_ext.state))
        self.assertContains(response, str(test_ext.locality))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_ext = add_extension()
        url = reverse('node:iden:state:extension:drop',
                      args=(test_ext.state.identity.node.id,
                            test_ext.state.identity.id,
                            test_ext.state.id,
                            test_ext.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_ext))


class ExtensionMethodTests(TestCase):

    def test_state_update(self):
        """
        Checks state update when a relation to it is added.
        """
        test_state = add_state()
        create = test_state.last_update
        add_extension(test_state=test_state)
        update = test_state.last_update
        self.assertLess(create, update)
