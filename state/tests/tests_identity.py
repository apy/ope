# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from identity.tests import \
    add_id, \
    add_indicator
from tree.tests import \
    add_node


class IdentityViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        node = add_node()
        response = self.client.get(reverse('node:iden:add',
                                           args=(node.id,)))
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_id = add_id()
        response = self.client.get(reverse('node:iden:edit',
                                           args=(test_id.node.id,
                                                 test_id.id,)))
        self.assertContains(response, test_id.label)
        self.assertContains(response, test_id.anteriority)
        self.assertContains(response, test_id.purpose)


class IndicatorViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_id = add_id()
        response = self.client.get(reverse('node:iden:indi:add',
                                           args=(test_id.node.id,
                                                 test_id.id,)))
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_ind = add_indicator()
        response = self.client.get(reverse('node:iden:indi:edit',
                                           args=(test_ind.identity.node.id,
                                                 test_ind.identity.id,
                                                 test_ind.id,)))
        self.assertContains(response, str(test_ind))
        self.assertContains(response, test_ind.label)

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_ind = add_indicator()
        response = self.client.get(reverse('node:iden:indi:drop',
                                           args=(test_ind.identity.node.id,
                                                 test_ind.identity.id,
                                                 test_ind.id,)))
        self.assertContains(response, str(test_ind))
