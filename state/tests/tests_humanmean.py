# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random
from django.test import \
    TestCase
from django.urls import \
    reverse
from state.models import \
    HumanMean, \
    HumanMeanType
from state.tests.tests_state import \
    rnd_str, \
    add_state
from job.tests import \
    add_history


def add_type(test_label=None):
    if test_label is None:
        test_label = \
            rnd_str(HumanMeanType._meta.get_field('label').max_length)
    return HumanMeanType.objects.create(label=test_label)


def add_humanmean(test_state=None,
                  test_history=None,
                  test_type=None,
                  test_days_nb=None):
    if test_state is None:
        test_state = add_state()
    if test_history is None:
        test_history = add_history()
    if test_type is None:
        test_type = add_type()
    if test_days_nb is None:
        test_days_nb = random.randint(0, 365)
    return HumanMean.objects.create(state=test_state,
                                    history=test_history,
                                    type=test_type,
                                    full_time_days_nb=test_days_nb)


class HumanMeanViewTests(TestCase):

    def test_add(self):
        """
        Checks the response to an add request.
        """
        test_state = add_state()
        url = reverse('node:iden:state:humanmean:add',
                      args=(test_state.identity.node.id,
                            test_state.identity.id,
                            test_state.id))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_hm = add_humanmean()
        url = reverse('node:iden:state:humanmean:edit',
                      args=(test_hm.state.identity.node.id,
                            test_hm.state.identity.id,
                            test_hm.state.id,
                            test_hm.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_hm.state))
        self.assertContains(response, str(test_hm.history))
        self.assertContains(response, str(test_hm.type))
        self.assertContains(response, str(test_hm.full_time_days_nb))

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_hm = add_humanmean()
        url = reverse('node:iden:state:humanmean:drop',
                      args=(test_hm.state.identity.node.id,
                            test_hm.state.identity.id,
                            test_hm.state.id,
                            test_hm.id))
        response = self.client.get(url)
        self.assertContains(response, str(test_hm))


class HumanMeanMethodTests(TestCase):

    def test_state_update(self):
        """
        Checks state update when a relation to it is added.
        """
        test_state = add_state()
        create = test_state.last_update
        add_humanmean(test_state=test_state)
        update = test_state.last_update
        self.assertLess(create, update)
