# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from django.db.models.signals import \
    post_save
from state.models import \
    AccountLine, \
    Extension, \
    Funding, \
    HumanMean, \
    Partnership, \
    Result, \
    Step


def update_state(sender, **kwargs):
    kwargs['instance'].state.last_update = datetime.datetime.now()
    kwargs['instance'].state.save()


post_save.connect(update_state, sender=AccountLine,
                  dispatch_uid="add_accountline_update_state")
post_save.connect(update_state, sender=Extension,
                  dispatch_uid="add_extension_update_state")
post_save.connect(update_state, sender=Funding,
                  dispatch_uid="add_funding_update_state")
post_save.connect(update_state, sender=HumanMean,
                  dispatch_uid="add_humanmean_update_state")
post_save.connect(update_state, sender=Partnership,
                  dispatch_uid="add_partnership_update_state")
post_save.connect(update_state, sender=Result,
                  dispatch_uid="add_result_update_state")
post_save.connect(update_state, sender=Step,
                  dispatch_uid="add_step_update_state")
