# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import \
    reverse_lazy
from django.views.generic import \
    DetailView, \
    ListView
from django.views.generic.edit import \
    CreateView, \
    DeleteView, \
    UpdateView
from partner.models import \
    Partner
from partner.forms import \
    PartnerForm


class PartnerList(ListView):
    model = Partner
    paginate_by = 20
    template_name = 'list.html'


class PartnerAdd(CreateView):
    model = Partner
    form_class = PartnerForm
    template_name = 'form.html'


class PartnerSee(DetailView):
    model = Partner


class PartnerEdit(UpdateView):
    model = Partner
    form_class = PartnerForm
    template_name = 'form.html'


class PartnerDrop(DeleteView):
    model = Partner
    success_url = reverse_lazy('partner:list')
    template_name = 'drop.html'
