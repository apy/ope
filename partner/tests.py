# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from django.urls import \
    reverse
from partner.models import \
    Partner
from tree.tests import \
    rnd_str


def add_partner(test_name=None):
    if test_name is None:
        test_name = rnd_str(Partner._meta.get_field('name').max_length)
    return Partner.objects.create(name=test_name)


class PartnerViewTests(TestCase):

    def test_list(self):
        """
        Checks the response to a list request.
        """
        response = self.client.get(reverse('partner:list'))
        self.assertEqual(response.status_code, 200)

    def test_add(self):
        """
        Checks the response to an add request.
        """
        response = self.client.get(reverse('partner:add'))
        self.assertEqual(response.status_code, 200)

    def test_see(self):
        """
        Checks the response to a see request.
        """
        test_partner = add_partner()
        response = self.client.get(reverse('partner:see',
                                           args=(test_partner.id,)))
        self.assertContains(response, test_partner.name)

    def test_edit(self):
        """
        Checks the response to an edit request.
        """
        test_partner = add_partner()
        response = self.client.get(reverse('partner:edit',
                                           args=(test_partner.id,)))
        self.assertContains(response, test_partner.name)

    def test_drop(self):
        """
        Checks the response to a drop request.
        """
        test_partner = add_partner()
        response = self.client.get(reverse('partner:drop',
                                           args=(test_partner.id,)))
        self.assertContains(response, str(test_partner))
