# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('tree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Identity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='verbose label', max_length=150, help_text='The verbose label extends the base label by a very short description.')),
                ('anteriority', models.CharField(verbose_name='anteriority', max_length=5000, help_text='The anteriority is a succinct description of previous operations on the same domain.')),
                ('purpose', models.CharField(verbose_name='long-term purpose', max_length=5000, help_text='The long-term purpose represents the design of the future advocated by the operation.')),
                ('node', mptt.fields.TreeForeignKey(verbose_name='node', to='tree.Node', help_text='The park operation node from which to build up this identity.', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'identity',
                'verbose_name_plural': 'identities',
            },
        ),
        migrations.CreateModel(
            name='Indicator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(verbose_name='label', max_length=200, help_text='An indicator represents a unit of evaluation and measure for the wanted results of the operation.')),
                ('identity', models.ForeignKey(verbose_name='identity', to='identity.Identity', help_text='The operation identity concerned by this indicator.', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'indicator',
                'verbose_name_plural': 'indicators',
            },
        ),
    ]
