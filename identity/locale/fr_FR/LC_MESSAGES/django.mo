��          �      ,      �  )   �  #   �  &   �  0   �     /     H  ?   M     �  
   �     �  	   �  
   �     �     �     �     �  �  �  e   t  2   �  )     <   7  "   t     �  I   �     �     �       
   7     B  	   N  %   X     ~     �                     	   
                                                      Future design advocated by the operation. Measure unit name of the indicator. Operation concerned by this indicator. Operation concerned by this static informations. Operation extended name. Save Succinct description of previous operations on the same domain. anteriority identities identity indicator indicators label long-term purpose node verbose label Project-Id-Version: 2016
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-31 12:53+0100
PO-Revision-Date: 2016-01-28 11:48+0100
Last-Translator: Adrien Panay <adrien.panay@free.fr>
Language-Team: français <>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Gtranslator 2.91.7
 Stratégie et objectifs à long terme représentant la conception du futur prônée par l'opération. Intitulé de l'unité de mesure de cet indicateur. Opération concernée par cet indicateur. Opération concernée par ces informations complémentaires. Intitulé étendu de l'opération. Enregistrer Description succincte des opérations précédentes sur le même domaine. antériorité informations complémentaires informations complémentaires indicateur indicateurs intitulé stratégie et objectifs à long-terme élément de la Charte intitulé étendu 