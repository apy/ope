# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Admin site for identity module.
"""

from django.contrib import \
    admin
from identity.models import \
    Identity, \
    Indicator
from state.admin import \
    ResultInLine


class IndicatorInLine(admin.TabularInline):
    model = Indicator
    extra = 2


class IdentityAdmin(admin.ModelAdmin):
    list_display = ('node',)
    list_filter = ('node__type',)
    search_fields = ['node__label']
    inlines = [IndicatorInLine]


class IndicatorAdmin(admin.ModelAdmin):
    list_display = ('label', 'identity')
    list_filter = ('identity', 'identity__node__type')
    search_fields = ['label', 'identity__node__label']
    inlines = [ResultInLine]


admin.site.register(Identity, IdentityAdmin)
admin.site.register(Indicator, IndicatorAdmin)
