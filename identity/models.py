# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Describes the static informations of operation related to the park.
A static information doesn't evolve during an operation.
"""

from django.db import \
    models
from django.urls import \
    reverse
from django.utils.translation import \
    ugettext_lazy as _
from mptt.models import \
    TreeOneToOneField
from tree.models import \
    Node


class Identity(models.Model):

    """
    An identity gathers the static informations of an operation.
    """

    node = TreeOneToOneField(
        Node,
        verbose_name=_('node'),
        help_text=_(
            'Operation concerned by this static informations.'),
        on_delete=models.CASCADE)

    label = models.CharField(
        blank=True,
        max_length=150,
        verbose_name=_('verbose label'),
        help_text=_(
            'Operation extended name.'))

    anteriority = models.CharField(
        max_length=5000,
        verbose_name=_('anteriority'),
        help_text=_(
            'Succinct description of previous operations on the same '
            'domain.'))

    purpose = models.CharField(
        max_length=5000,
        verbose_name=_('long-term purpose'),
        help_text=_(
            'Future design advocated by the operation.'))

    class Meta:
        verbose_name = _('identity')
        verbose_name_plural = _('identities')

    def __str__(self):
        return str(self.node.short_str())

    def get_absolute_url(self):
        return reverse('node:see',
                       kwargs={'node_pk': self.node.id})


class Indicator(models.Model):

    """
    An indicator represents a measure unit concerning the results of an
    operation.
    """

    identity = models.ForeignKey(
        Identity,
        verbose_name=_('identity'),
        help_text=_(
            'Operation concerned by this indicator.'),
        on_delete=models.CASCADE)

    label = models.CharField(
        max_length=200,
        verbose_name=_('label'),
        help_text=_(
            'Measure unit name of the indicator.'))

    class Meta:
        verbose_name = _('indicator')
        verbose_name_plural = _('indicators')
        unique_together = (("identity", "label"),)

    def __str__(self):
        return self.label

    def get_absolute_url(self):
        return reverse('node:see',
                       kwargs={'node_pk': self.identity.node.id}) + \
               '#' + str(self._meta.verbose_name_plural)
