# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Describes the identity forms.
"""

from django.forms import \
    ModelForm, \
    Textarea
from django.utils.translation import \
    ugettext_lazy as _
from crispy_forms.helper import \
    FormHelper
from crispy_forms.layout import \
    Submit
from identity.models import \
    Identity, \
    Indicator


class IndicatorForm(ModelForm):
    class Meta:
        model = Indicator
        fields = [
            'identity',
            'label',
        ]

    def __init__(self, *args, **kwargs):
        super(IndicatorForm, self).__init__(*args, **kwargs)
        self.fields['identity'].empty_label = None
        self.fields['identity'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))


class IdentityForm(ModelForm):
    class Meta:
        model = Identity
        fields = [
            'node',
            'label',
            'anteriority',
            'purpose',
        ]
        widgets = {
            'anteriority': Textarea(),
            'purpose': Textarea(),
        }

    def __init__(self, *args, **kwargs):
        super(IdentityForm, self).__init__(*args, **kwargs)
        self.fields['node'].empty_label = None
        self.fields['node'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', _('Save')))
