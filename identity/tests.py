# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import \
    TestCase
from identity.models import \
    Identity, \
    Indicator
from tree.tests import \
    rnd_str, \
    add_node


def add_id(test_node=None,
           test_label=None,
           test_anteriority=None,
           test_purpose=None):
    if test_node is None:
        test_node = add_node(test_label=None)
    if test_label is None:
        test_label = \
            rnd_str(Identity._meta.get_field('label').max_length)
    if test_anteriority is None:
        test_anteriority = \
            rnd_str(Identity._meta.get_field('anteriority').max_length)
    if test_purpose is None:
        test_purpose = \
            rnd_str(Identity._meta.get_field('purpose').max_length)
    return Identity.objects.create(node=test_node,
                                   label=test_label,
                                   anteriority=test_anteriority,
                                   purpose=test_purpose)


def add_indicator(test_id=None,
                  test_label=None):
    if test_id is None:
        test_id = add_id()
    if test_label is None:
        test_label = \
            rnd_str(Indicator._meta.get_field('label').max_length)
    return Indicator.objects.create(identity=test_id,
                                    label=test_label)


class IndicatorMethodTests(TestCase):

    def test_str(self):
        """
        Checks str.
        """
        test_ind = add_indicator()
        self.assertEqual(str(test_ind), test_ind.label)
