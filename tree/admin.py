# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
contains models to be available in admin site
"""

from django.contrib import \
    admin
from django.utils.translation import \
    ugettext_lazy as _
from mptt.admin import \
    MPTTModelAdmin
from tree.models import \
    Node, \
    NodeType


class NodeAdmin(MPTTModelAdmin):
    list_display = ('label', 'type')
    list_filter = ('type',)
    search_fields = ['label']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "parent":
            str_node_id = request.path.split('/')[4]
            if str_node_id.isdigit():
                node_obj = Node.objects.get(id=int(str_node_id))
                kwargs["queryset"] = \
                    Node.objects.filter(type=node_obj.type.parent.id)
        return super(NodeAdmin,
                     self).formfield_for_foreignkey(db_field,
                                                    request,
                                                    **kwargs)

admin.site.site_header = _('OPE administration')
admin.site.register(NodeType)
admin.site.register(Node, NodeAdmin)
