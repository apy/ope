# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Content unit tests
"""

import string
import random
from django.test import \
    TestCase
from tree.models import \
    Node, \
    NodeType


def rnd_str(length=10,
            charset=string.ascii_letters + string.digits):
    """
    returns a random string of the given length and containing the
    given character set.
    """
    result = ''
    while len(result) < length:
        result += random.choice(charset)
    return result


def add_type(test_label=None):
    if test_label is None:
        test_label = rnd_str(NodeType._meta.get_field('label').max_length)
    return NodeType.objects.create(label=test_label)


def add_node(test_label=None,
             test_type=None,
             test_parent=None):
    if test_label is None:
        test_label = rnd_str(Node._meta.get_field('label').max_length)
    if test_type is None:
        test_type = add_type()
    return Node.objects.create(label=test_label,
                               type=test_type,
                               parent=test_parent)


class NodeMethodTests(TestCase):

    def test_str(self):
        """
        Checks str.
        """
        test_node = add_node()
        test_node_str = ''
        if test_node.get_code():
            test_node_str += test_node.get_code() + ' '
        test_node_str += test_node.label
        self.assertEqual(str(test_node),
                         test_node_str)

    def test_short_str(self):
        """
        Checks short str.
        """
        test_node = add_node()
        test_short_str = str(test_node.type) + ' '
        if test_node.get_code():
            test_short_str += test_node.get_code()
        else:
            test_short_str += test_node.label
        self.assertEqual(test_node.short_str(),
                         test_short_str)

    def test_no_sibling(self):
        """
        Checks get_prev_sibling_nb behavior with no sibling.
        """
        test_node = add_node()
        prev_sibling_nb = test_node.get_prev_sibling_nb()
        self.assertEqual(prev_sibling_nb, 0)

    def test_one_sibling(self):
        """
        Checks get_prev_sibling_nb behavior with one sibling.
        """
        test_root_node = add_node()
        add_node(test_parent=test_root_node)    # 1st sibling
        test_node = add_node(test_parent=test_root_node)
        prev_sibling_nb = test_node.get_prev_sibling_nb()
        self.assertEqual(prev_sibling_nb, 1)

    def test_two_sibling(self):
        """
        Checks get_prev_sibling_nb behavior with two sibling.
        """
        test_root_node = add_node()
        # 1st sibling
        add_node(test_parent=test_root_node)
        # 2nd sibling
        add_node(test_parent=test_root_node)
        test_node = add_node(test_parent=test_root_node)
        prev_sibling_nb = test_node.get_prev_sibling_nb()
        self.assertEqual(prev_sibling_nb, 2)

    def test_root_node_code(self):
        """
        Checks code of a root node.
        """
        test_node = add_node()
        code = test_node.get_code()
        self.assertEqual(code, '')

    def test_1st_lvl_node_code(self):
        """
        Checks code of a 1st level node.
        """
        test_root_node = add_node()
        test_node = add_node(test_parent=test_root_node)
        code = test_node.get_code()
        self.assertEqual(code, '1')

    def test_2nd_lvl_node_code(self):
        """
        Checks code of a 2nd level node.
        """
        test_root_node = add_node()
        test_1st_lvl_node = add_node(test_parent=test_root_node)
        test_node = add_node(test_parent=test_1st_lvl_node)
        code = test_node.get_code()
        self.assertEqual(code, '11')

    def test_3rd_lvl_node_code(self):
        """
        Checks code of a 3rd level node.
        """
        test_root_node = add_node()
        test_1st_lvl_node = add_node(test_parent=test_root_node)
        test_2nd_lvl_node = add_node(test_parent=test_1st_lvl_node)
        test_node = add_node(test_parent=test_2nd_lvl_node)
        code = test_node.get_code()
        self.assertEqual(code, '111')
