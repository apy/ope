# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Describes the hierarchical organization of operation related to the
park.
"""

from django.db import \
    models
from django.urls import \
    reverse
from django.utils.translation import \
    ugettext_lazy as _
from mptt.models import \
    MPTTModel, \
    TreeForeignKey


class NodeType(models.Model):

    """
    A node type represents the category of a node:
    charte, axe, objectif stratégique,
    objectif opérationnel, mesure, action.
    """

    label = models.CharField(
        max_length=25,
        unique=True,
        verbose_name=_('label'),
        help_text=_(
            'Unique name of the category of operation.'))

    child = models.OneToOneField(
        'self',
        null=True,
        blank=True,
        related_name='parent',
        verbose_name=_('child'),
        help_text=_(
            'Child node type of current node type.'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('node type')
        verbose_name_plural = _('node types')

    def __str__(self):
        return self.label

    def pluralize_label(self):
        return self.label.replace(' ', 's ', 1) + 's'


class Node(MPTTModel):

    """
    A node represents the hierarchical part of an operation.
    """

    label = models.CharField(
        max_length=250,
        unique=True,
        verbose_name=_('label'),
        help_text=_(
            'Unique name of the operation.'))

    type = models.ForeignKey(
        NodeType,
        verbose_name=_('type'),
        help_text=_(
            'Category of the operation.'),
        on_delete=models.CASCADE)

    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True,
        verbose_name=_('parent'),
        help_text=_(
            'Parent operation of the operation.'),
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('node')
        verbose_name_plural = _('nodes')

    def __str__(self):
        result = ''
        node_code = self.get_code()
        if node_code:
            result += node_code + ' '
        result += self.label
        return result

    def short_str(self):
        result = str(self.type) + ' '
        node_code = self.get_code()
        if node_code:
            result += node_code
        else:
            result += self.label
        return result

    def get_absolute_url(self):
        return reverse('node:see', kwargs={'node_pk': self.id})

    def get_code(self):
        """
        Get the code string.
        Examples:
        '1' or '2' for 'axe',
        '11' or '23' for 'objectif strategique',
        '111' or '232' for 'objectif operationel',
        '1111' or '2324' for 'mesure',
        '' for any other type of node
        :return a string.
        """
        result = ''
        if not(self.is_root_node() or (self.type.id == 6)):
            # get parent code
            result = self.parent.get_code()
            result += str(self.get_prev_sibling_nb() + 1)
        return result

    def get_prev_sibling_nb(self, prev_sibling_nb=None):
        """
        Get an integer of the number of previous sibling.
        :param prev_sibling_nb: the number of previous sibling of the
        next sibling.
        """
        if prev_sibling_nb is None:
            prev_sibling_nb = 0
        result = prev_sibling_nb
        previous_sibling = self.get_previous_sibling()
        if previous_sibling:
            result += 1
            result = previous_sibling.get_prev_sibling_nb(result)
        return result
