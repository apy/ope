# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('label', models.CharField(verbose_name='label', unique=True, max_length=250, help_text='The label is a name wich is unique among node names.')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(verbose_name='parent', to='tree.Node', help_text='The parent is the node from wich it depends.', related_name='children', blank=True, null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'node',
                'verbose_name_plural': 'nodes',
            },
        ),
        migrations.CreateModel(
            name='NodeType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('label', models.CharField(verbose_name='label', unique=True, max_length=25, help_text='The label is a name wich is unique among node type names.')),
            ],
            options={
                'verbose_name': 'node type',
                'verbose_name_plural': 'node types',
            },
        ),
        migrations.AddField(
            model_name='node',
            name='type',
            field=models.ForeignKey(verbose_name='type', to='tree.NodeType', help_text='The type is a category of nodes which shares a common nature.', on_delete=models.CASCADE),
        ),
    ]
