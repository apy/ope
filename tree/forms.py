# ope -- stands for "Outil de Préparation à l'Évaluation" in french
# Copyright 2015 ope contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import \
    ModelForm
from django.utils.translation import \
    ugettext_lazy as _
from crispy_forms.helper import \
    FormHelper
from crispy_forms.layout import \
    Submit
from tree.models import \
    Node, \
    NodeType


class NodeTypeForm(ModelForm):
    class Meta:
        model = NodeType
        fields = [
            'label',
        ]

    def __init__(self, *args, **kwargs):
        super(NodeTypeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'

        self.helper.add_input(Submit('submit', _('Save')))


class NodeForm(ModelForm):
    class Meta:
        model = Node
        fields = [
            'parent',
            'type',
            'label',
        ]

    def __init__(self, *args, **kwargs):
        super(NodeForm, self).__init__(*args, **kwargs)
        self.fields['parent'].empty_label = None
        self.fields['parent'].widget.attrs['readonly'] = True
        self.fields['type'].empty_label = None
        self.fields['type'].widget.attrs['readonly'] = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        # self.helper.layout = Layout(
        #     Field('parent', disabled=Disabled), # readonly=True),
        #     Field('type', disabled=Disabled),   # readonly=True),
        #     'label',
        #     FormActions(
        #         Submit('save', _('Save')),
        #         Button('cancel', _('Cancel')))
        # )
        self.helper.add_input(Submit('submit', _('Save')))
